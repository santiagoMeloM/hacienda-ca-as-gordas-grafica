
import SphereObject from './object/sphere.js';
import CubeObject from './object/cube.js';
import LampLight from './light/lamp.js';
import CustomObject from './object/custom.js';

export default class Main {
  constructor() {
    this.scene = new THREE.Scene();
    this.aspect = window.innerWidth / window.innerHeight;
    this.camera = new THREE.PerspectiveCamera(75, this.aspect, 0.1, 1000);
    this.camera.position.z = 5;
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(this.renderer.domElement);
    this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
    this.objects = new Map();
    this.textures = new Map();
    this.loadTextures();
    this.loadObjects();
    this.render();
  }

  loadTextures() {
    this.textures.set("tierra", new THREE.TextureLoader().load("src/asset/tierra.jpg"));
    this.textures.set("nm", new THREE.TextureLoader().load("src/asset/nm.png"));
  }

  addToScene(mesh, name) {
    this.objects.set(name, mesh);
    this.scene.add(this.objects.get(name).getObject());
  }

  loadObjects() {
    var light = new LampLight(0xfffff, 2, {x:-20, y:10, z:10});
    this.objects.set("light", light);

    var sphere = new SphereObject(0.5, 24, 16, {
      color: 0xaaaaaa,
      specular: 0x333333,
      shininess: 15,
      map: this.textures.get("tierra"),
      normalMap: this.textures.get("nm") });
    this.objects.set("sphere", sphere);

    var cube = new CubeObject(1, 1, 1);
    this.objects.set("cube", cube);


    var dog = new CustomObject({
      name: "dog",
      jsonUrl: "src/asset/json/dog.json",
      texture: this.textures.get("nm"),
      addScene: this.addToScene.bind(this)
    });

    this.scene.add(this.objects.get("light").getLight());
    this.scene.add(this.objects.get("sphere").getObject());
    this.scene.add(this.objects.get("cube").getObject());
  }

  render() {
    requestAnimationFrame(this.render.bind(this));
    if (this.objects.has("dog"))
      this.objects.get("dog").rotate(0.05, 0.05, 0);
    this.objects.get("cube").rotate(-0.05, 0.05, 0);
    this.objects.get("sphere").rotate(0.005, 0.005, 0.005);
    this.renderer.render(this.scene, this.camera);
  }

}
