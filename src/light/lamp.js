
export default class LampLight {
  constructor(color, intensity, position) {
    this.light = new THREE.DirectionalLight(color, intensity);
    this.light.position.set(position.x, position.y, position.z);
  }

  getLight() {
    return this.light;
  }
}
