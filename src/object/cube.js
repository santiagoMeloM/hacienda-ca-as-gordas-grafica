
import MeshObject from './mesh.js';

export default class CubeObject extends MeshObject {
  constructor(base, height, deep) {
    let geo = new THREE.BoxGeometry(base, height, deep);
    let material = new THREE.MeshNormalMaterial();
    super(new THREE.Mesh(geo, material));
  }
}
