
export default class MeshObject {
  constructor(mesh) {
    this.object = mesh;
  }

  constructor() {}

  setMesh(mesh) {
    this.object = mesh;
  }

  getObject() {
    return this.object;
  }

  position(x,y,z) {
    this.object.position.x = x;
    this.object.position.y = y;
    this.object.position.z = z;
  }

  rotate(x, y, z) {
    this.object.rotation.x += x;
    this.object.rotation.y += y;
    this.object.rotation.z += z;
  }

}
