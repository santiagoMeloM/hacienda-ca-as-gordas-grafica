
import MeshObject from './mesh.js';

export default class SphereObject extends MeshObject {
  constructor(radius, width, height, meshCharac) {
    let geo =  new THREE.SphereGeometry(radius, width, height);
    geo.translate(-4,0,0);
    let material = new THREE.MeshPhongMaterial(meshCharac);
    super(new THREE.Mesh(geo, material));
  }
}
