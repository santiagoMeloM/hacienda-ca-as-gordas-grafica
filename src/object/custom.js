
import MeshObject from './mesh.js';

export default class CumtomObject extends MeshObject {
  constructor(specs) {
    super();
    var loader = new THREE.ObjectLoader();
    let material = new THREE.MeshPhongMaterial({ map: specs.texture });
    loader.load(specs.jsonUrl, (obj)=> {
      let mesh = obj.children[0];
      mesh.material = material;
      this.object = mesh;
      specs.addScene(this, specs.name);
    });
  }
}
